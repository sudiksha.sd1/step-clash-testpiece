﻿using UnityEngine;

namespace StepClash.Utils
{
    public class BaseClass : MonoBehaviour
    {
        #region --------------------------------- Private Methods --------------------------------------------------
        protected T SetView<T>(GameObject view)
        {
            return view.GetComponentInChildren<T>();
        }

        protected T SetController<T>(GameObject controller)
        {
            return controller.GetComponentInChildren<T>();
        }
        #endregion -------------------------------------------------------------------------------------------------
    }
}