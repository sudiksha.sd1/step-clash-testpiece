﻿namespace StepClash.Constant
{
    public static class GameConstants
    {
        public const int totalPlayers = 4;
        public const int totalGameTime = 900; // 900 seconds or 15min
        public const int totalStartPoint = 25;
        public const float winningPoints = 40;
        public const float durationToUpdatePoint = 20;
        public const int enemyTerritorySnatchDuration = 20;
        public const int selfPointsLostDUration = 60;
        public const int opponentRandomFunctionDurationStartingRange = 60;
        public const int opponentRandomFunctionDurationEndingRange = 120;
        public const int opponentFunctionDuration = 32;
        public const int notificationDuration = 5;
    }
}
