﻿using UnityEngine;
using StepClash.Utils;
using StepClash.Views;
using StepClash.Models;
using StepClash.Constant;
using System.Collections;
using StepClash.Managers;
using System.Collections.Generic;


namespace StepClash.Controllers
{
    public class StepperController : BaseClass
    {
        #region ---------------------------- Private Variables ----------------------------
        private GameController gameController;
        private Dictionary<string, StepperData> steppers;
        private List<StepperUI> stepperViews;
        private List<StepperData> stepperDatas;
        #endregion --------------------------------------------------------


        #region ---------------------------- Private Methods ----------------------------
        private void Start()
        {
            this.SetController<StepperController>(this.gameObject);
            gameController = this.gameObject.GetComponent<GameController>();
            stepperDatas = new List<StepperData>();
            stepperViews = new List<StepperUI>();
            steppers = new Dictionary<string, StepperData>();
        }
        #endregion --------------------------------------------------------


        #region ---------------------------- Public Methods ----------------------------
        // Creater models and fill data
        public void CreateSteppers()
        {
       //     yield return new WaitForEndOfFrame();
            for (int i = 0;i<GameConstants.totalPlayers;i++)
            {
                var tileName = LocationManager.instance.activeTiles[i].ToString();
                var stepperData = new StepperData();
                stepperViews.Add(new StepperUI());
                if (i == 0)
                {
                    stepperData.SetStepperData(i, true, tileName,true, GameConstants.totalStartPoint,false,false,i);
                    steppers.Add(tileName, stepperData);
                }
                else
                {
                    stepperData.SetStepperData(i, false, tileName,true, GameConstants.totalStartPoint,false,false,i);
                    steppers.Add(tileName, stepperData);
                }
                stepperDatas.Add(stepperData);
                gameController.UpdateScoreText(i, stepperData.currentPoints);
            }
        }

        public void SetUpTileMaterial()
        {
            foreach (var unityTiles in LocationManager.instance.GetMap().MapVisualizer.ActiveTiles)
            {
                if (GameManager.instance.IsTileActive(unityTiles.Value.UnwrappedTileId.ToString()))
                {
                    var stepper = GameManager.instance.GetStepperFromTile(unityTiles.Value.CanonicalTileId.ToString());
                    Color stepperColor;
                    var colorEnum = (StepClash.Models.StepperColor)stepper.stepperColorIndex;
                    ColorUtility.TryParseHtmlString(colorEnum.ToString(), out stepperColor);
                    unityTiles.Value.gameObject.GetComponent<Renderer>().material.color = stepperColor;
                }
            }
        }

        // Handling logic when the player changes the tile or area. Depending on the current tile the player stepped on, it may earn or get its point deducted.
        public void ChangeTile()
        {
            // check for tile change
            // check if the tile is unkonwn, enemy or own tile
            // Random stepper will go to random area after certain amount of time; make it a couroutine
            var tileName = LocationManager.instance.curTile.ToString();
            // if I went to enemy tile, I'll snatch points
            stepperDatas[0].isInOwnTerritory = false;
            if (steppers.ContainsKey(tileName))
            {
                if (!steppers[tileName].isPlayer)
                {
                    gameController.Notify("You're entering " + (StepperColor)steppers[tileName].stepperColorIndex + "'s area !!");
                    StartCoroutine(SnatchPoints( 0,  tileName, GameConstants.enemyTerritorySnatchDuration,  1));
                }
                else
                {
                    gameController.Notify("You're entering your own area !");
                    stepperDatas[0].isInOwnTerritory = true;
                    StartCoroutine(SnatchMultiplePoints( 0, tileName, GameConstants.enemyTerritorySnatchDuration,  1));
                    StartCoroutine(GiveUpPoints( 0,  tileName, GameConstants.selfPointsLostDUration,  1));
                }
            }
            else
            {
                gameController.Notify("You're entering common area !");
            }
                // if I come back to my tile, I'll snatch points based on current number of enemies
                // Same goes for enemy
        }

        // Snatching points for enemyStepper based on the duration it is being on that particular area
        public IEnumerator SnatchPoints(int player1, string player2, int duration, int scoreToUpdate)
        {
            while (!stepperDatas[player1].isInOwnTerritory && LocationManager.instance.curTile.ToString() == player2)
            {
                if(!steppers[player2].isInOwnTerritory)
                {
                    stepperDatas[player1].currentPoints += scoreToUpdate;
                    steppers[player2].currentPoints -= scoreToUpdate;
                }
                else
                {
                    stepperDatas[player1].currentPoints -= scoreToUpdate;
                    steppers[player2].currentPoints += scoreToUpdate;
                }
                var go = IsGameOver();
                if (go != -1)
                    gameController.OnGameOver(go);
                gameController.UpdateScoreText(player1, stepperDatas[player1].currentPoints);
                gameController.UpdateScoreText(steppers[player2].stepperColorIndex, stepperDatas[player1].currentPoints);
                yield return new WaitForSeconds(duration);
                //check game over and change status and display panel
            }
        }

        // return winner player's index if game is over, else return -1
        public int IsGameOver()
        {
            for (int i=0;i<GameConstants.totalPlayers;i++)
            {
                if (stepperDatas[i].currentPoints >= GameConstants.winningPoints)
                    return i;
            }
            return -1;
        }


        // Check for winner
        public int CheckForTheWinner()
        {
            int maxScore = 0,win=-1;
            for (int i = 0; i < GameConstants.totalPlayers; i++)
            {
                if (maxScore < stepperDatas[i].currentPoints)
                {
                    maxScore = stepperDatas[i].currentPoints;
                    win = i;
                }
            }
            return win;
        }

        // BETTER USE GETTER AND SETTER !
        public StepperData GetStepper(int index)
        {
            return stepperDatas[index];
        }
        public StepperData GetStepperFromTile(string tile)
        {
            return steppers[tile];
        }
        public bool IsTileActive(string tile)
        {
            return steppers.ContainsKey(tile);
        }

        // Snatch multiple points if multiple enemies are on my territory
        public IEnumerator SnatchMultiplePoints(int player1, string player2, int duration, int scoreToUpdate)
        {
            while (stepperDatas[player1].isInOwnTerritory)
            {
                stepperDatas[player1].currentPoints += (scoreToUpdate * stepperDatas[player1].enemyCountInTerritory.Count);
                var go = IsGameOver();
                if (go != -1)
                    gameController.OnGameOver(go);
                gameController.UpdateScoreText(player1, stepperDatas[player1].currentPoints);
                yield return new WaitForSeconds(duration);
                //check game over and change status and display panel
            }
        }

        public IEnumerator GiveUpPoints(int player1, string player2, int duration, int scoreToUpdate)
        {
            while (stepperDatas[player1].isInOwnTerritory)
            {
                stepperDatas[player1].currentPoints -= 1;
                gameController.UpdateScoreText(player1, stepperDatas[player1].currentPoints);
                var go = IsGameOver();
                if (go != -1)
                    gameController.OnGameOver(go);
                yield return new WaitForSeconds(duration);
                //check game over and change status and display panel
            }
        }

        // Initiate enemy stepper's stepping other area activity (**Quick hack)
        public IEnumerator StartOpponentStepperFunctionality(float duration)
        {
            float curTimer = duration;
            while (GameManager.instance.IsInGame)
            {
                curTimer -= Time.deltaTime;
                if (curTimer <= 0)
                {
                    curTimer = duration;
                    StartCoroutine(SendOpponentToATerritory(Random.Range(1,GameConstants.totalPlayers),Random.Range(0,GameConstants.totalPlayers),Random.Range(GameConstants.opponentRandomFunctionDurationStartingRange, GameConstants.opponentRandomFunctionDurationEndingRange)));
                }
                yield return null;
            }
        }

        public IEnumerator SendOpponentToATerritory(int attacker, int attackee, int duration)
        {
            float curTimer = duration;
            stepperDatas[attacker].isInOwnTerritory = false;
            if (attackee != attacker)
            {
                if (attackee == 0)
                {
                    gameController.Notify((StepperColor)stepperDatas[attacker].stepperColorIndex + " is in your area !!");
                    if (!stepperDatas[attackee].enemyCountInTerritory.Contains(attacker))
                     stepperDatas[attackee].enemyCountInTerritory.Add(attacker);
                    gameController.UpdateEnemyCountText(stepperDatas[attackee].enemyCountInTerritory.Count);
                }
                else
                {

                    gameController.Notify((StepperColor)stepperDatas[attacker].stepperColorIndex + " is in "+ (StepperColor)stepperDatas[attackee].stepperColorIndex+"'s area !! ");
                }
                if (stepperDatas[attackee].isInOwnTerritory)
                {
                    stepperDatas[attackee].currentPoints += duration / GameConstants.enemyTerritorySnatchDuration;
                    stepperDatas[attacker].currentPoints -= duration / GameConstants.enemyTerritorySnatchDuration;
                }
                else
                {
                    stepperDatas[attackee].currentPoints -= duration / GameConstants.enemyTerritorySnatchDuration;
                    stepperDatas[attacker].currentPoints += duration / GameConstants.enemyTerritorySnatchDuration;
                }
                gameController.UpdateScoreText(attacker, stepperDatas[attacker].currentPoints);
                gameController.UpdateScoreText(attackee, stepperDatas[attackee].currentPoints);
            }
            else
            {
                gameController.Notify((StepperColor)stepperDatas[attackee].stepperColorIndex + " is  now in his own Area !!");
            }
            var go = IsGameOver();
            if (go != -1)
                gameController.OnGameOver(go);
            while (GameManager.instance.IsInGame && curTimer>0)
            {
                curTimer -= Time.deltaTime;
                stepperDatas[attacker].isInOwnTerritory = true;
            yield return null;
            }
            if (attackee == 0)
            {
                gameController.Notify((StepperColor)stepperDatas[attacker].stepperColorIndex + " left your area !!");
                stepperDatas[attackee].enemyCountInTerritory.Remove(attacker);
                gameController.UpdateEnemyCountText(stepperDatas[attackee].enemyCountInTerritory.Count);
            }
                //check game over and change status and display panel
        }
        #endregion --------------------------------------------------------
    }
}
