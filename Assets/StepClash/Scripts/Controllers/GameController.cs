﻿using UnityEngine;
using StepClash.Views;
using StepClash.Utils;
using StepClash.Models;
using System.Collections;
using StepClash.Constant;
using StepClash.Managers;


namespace StepClash.Controllers
{
    public class GameController : BaseClass
    {
        #region ---------------------------- Private Variables ----------------------------
        private GameUI gameUI;
        private StepperController stepperController;
        #endregion --------------------------------------------------------

        #region ---------------------------- Private Methods ----------------------------
        private void Start()
        {
            this.SetController<GameController>(this.gameObject);
            stepperController = this.gameObject.GetComponent<StepperController>();
            gameUI = GameObject.Find("GameView").GetComponent<GameUI>();
            gameUI.Init(this);
        }

        // Starting game timer
        private IEnumerator StartGameTimer()
        {
            float curTime = GameConstants.totalGameTime;
            while (curTime > 0)
            {
                curTime -= Time.deltaTime;
                gameUI.UpdateTimerText(curTime);
                yield return null;
            }
            OnGameOver(stepperController.CheckForTheWinner());
        }
        #endregion --------------------------------------------------------

        #region ---------------------------- Application Service Methods ----------------------------
        private void OnApplicationQuit()
        {
            if (GameManager.instance.IsInGame)
            {
                int winner = stepperController.CheckForTheWinner();
                var str1 = "Game Closed..";
                var str2 = "Anyways, I declare " + (StepperColor)stepperController.GetStepper(winner).stepperColorIndex + " the WINNER..";
                NotificationManager.instance.CreateNotification(NotificationManager.instance.CreateChannel(), str1, str2);
            }
            else
            {
                var str1 = "GameClosed";
                var str2 = "At least you should have started..";
                NotificationManager.instance.CreateNotification(NotificationManager.instance.CreateChannel(), str1, str2);
            }
        }

        private void OnApplicationFocus(bool status)
        {
            if (!status && GameManager.instance.IsInGame)
            {
                int winner = stepperController.CheckForTheWinner();
                var str1 = "Get back in the game before you lose, or app closes...";
                var str2 = "Looks like " + (StepperColor)stepperController.GetStepper(winner).stepperColorIndex + " is leading its steps";
                NotificationManager.instance.CreateNotification(NotificationManager.instance.CreateChannel(), str1, str2);
            }
            else if (status && GameManager.instance.IsInGame)
            {
                int winner = stepperController.CheckForTheWinner();
                var str1 = "Yaay, we're back in the game...";
                var str2 = "Let's steal some steps";
                NotificationManager.instance.CreateNotification(NotificationManager.instance.CreateChannel(), str1, str2);
            }
        }

        #endregion --------------------------------------------------------


        #region ---------------------------- Public Methods ----------------------------

        public void StartNewGameSession()
        {
            GameManager.instance.IsInGame = true;
            LocationManager.instance.SetLocation();
            LocationManager.instance.SetMap();
            // StartCoroutine(stepperController.CreateSteppers());
            stepperController.CreateSteppers(); 
            stepperController.SetUpTileMaterial();
            StartCoroutine(StartGameTimer());
            StartCoroutine(stepperController.StartOpponentStepperFunctionality(GameConstants.opponentFunctionDuration));
        }
        
        public void Notify(string notification)
        {
            gameUI.SetNotification(notification);
        }

        public void UpdateScoreText(int index, int score)
        {
            gameUI.UpdateScoreText(index, score);
        }

        public void UpdateEnemyCountText(int count)
        {
            gameUI.UpdateEnemyCountText(count);
        }

        public void OnGameOver(int i)
        {
            string winnerText = "Winner is " + (StepperColor)stepperController.GetStepper(i).stepperColorIndex + " with " + (StepperColor)stepperController.GetStepper(i).currentPoints + " points !!";
            GameManager.instance.IsInGame = false;
            gameUI.SetGameOverScreen();
            gameUI.SetGameOverText(winnerText);
        }

        #endregion --------------------------------------------------------
    }
}
