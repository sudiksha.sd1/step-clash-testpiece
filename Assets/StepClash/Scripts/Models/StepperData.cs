﻿using UnityEngine;
using System.Collections.Generic;

namespace StepClash.Models
{
    public class StepperData : MonoBehaviour
    {
        #region ---------------------------- Public Variables ----------------------------
        public bool isPlayer;
        public bool isInGame;
        public int stepperColorIndex;
        public int homeTileIndex;
        public int currentTileIndex;
        public string stepperTileName;
        public int currentPoints;
        public bool isInOwnTerritory;
        public bool isInOtherTerritory;
        public List<int> enemyCountInTerritory;
        #endregion --------------------------------------------------------


        #region ---------------------------- Public Methods ----------------------------
        public void SetStepperData(int triadColor, bool isCurPlayer, string tileName,bool inGame, int curPoints,bool canStealPoints, bool isProneToAttack,int homeTile)
        {
            stepperColorIndex = triadColor;
            isPlayer = isCurPlayer;
            stepperTileName = tileName;
            isInGame = inGame;
            currentPoints = curPoints;
            this.isInOtherTerritory = canStealPoints;
            this.isInOwnTerritory = true;
            homeTileIndex = homeTile;
            enemyCountInTerritory = new List<int>();
        }

        // USE GETTERS SETTERS !!


        public void SetCurrentTile(int curTile)
        {
            currentTileIndex = curTile;
        }

        public void AddEnemy(int stepperID)
        {
            enemyCountInTerritory.Add(stepperID);
        }

        public void RemoveEnemy(int stepperID)
        {
            enemyCountInTerritory.Remove(stepperID);
        }


        public int GetEnemyCount()
        {
            return enemyCountInTerritory.Count;
        }
    }
    #endregion --------------------------------------------------------


    #region ---------------------------- Enum ----------------------------
    public enum StepperColor
    {
        red,
        blue,
        green,
        yellow
    }
    #endregion --------------------------------------------------------
}