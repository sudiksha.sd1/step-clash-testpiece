﻿using UnityEngine;
using UnityEngine.UI;
using StepClash.Utils;
using StepClash.Managers;
using StepClash.Controllers;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

namespace StepClash.Views
{
    public class GameUI : BaseClass
    {

        #region ---------------------------- Private UI Variables ----------------------------
        [SerializeField] private Text notificationText;
        [SerializeField] private Text gameTimeText;
        [SerializeField] private Text winnerGameText;
        [SerializeField] private Text enemyInTerritoryCountText;
        [SerializeField] private Button newGameBtn;
        [SerializeField] private Button statsBtn;
        [SerializeField] private Button statsToGameBtn;
        [SerializeField] private Button resetGameBtn;
        [SerializeField] private List<Text> steppersScoreText;
        #endregion --------------------------------------------------------

        #region ---------------------------- Private Variables ----------------------------
        [SerializeField] private GameObject menuPanel;
        [SerializeField] private GameObject statsPanel;
        [SerializeField] private GameObject gamePanel;
        [SerializeField] private GameObject gameOverPanel;
        private GameController gameController;
        #endregion --------------------------------------------------------


        #region ---------------------------- Private Methods ----------------------------
        private void SetListener()
        {
            newGameBtn.onClick.AddListener(StartNewGame);
            statsBtn.onClick.AddListener(OpenStatsWindow);
            statsToGameBtn.onClick.AddListener(BackToGame);
            resetGameBtn.onClick.AddListener(ResetGame);
        }


        private void StartNewGame()
        {
            if (GameManager.instance.isMapInitialized)
            {
                menuPanel.SetActive(false);
                gamePanel.SetActive(true);
                gameController.StartNewGameSession();
            }
        }

        private void OpenStatsWindow()
        {
            statsPanel.SetActive(true);
            gamePanel.SetActive(false);
        }

        private void BackToGame()
        {
            statsPanel.SetActive(false);
            gamePanel.SetActive(true);
        }

        private void ResetGame()
        {
            SceneManager.LoadScene("Main");
        }

        private void SetView()
        {
            this.SetView<GameUI>(this.gameObject);
        }
        #endregion --------------------------------------------------------


        #region ---------------------------- Public Methods ----------------------------
        public void Init(GameController gameController)
        {
            this.gameController = gameController;
            SetListener();
            SetView();
        }

        public void SetGameOverScreen()
        {
            gameOverPanel.SetActive(true);
            gamePanel.SetActive(false);
        }

        public void SetNotification(string notification)
        {
            notificationText.text = notification;
        }

        public void SetGameOverText(string winnerText)
        {
            winnerGameText.text = winnerText;
        }

        public void UpdateScoreText(int index, int score)
        {
            steppersScoreText[index].text = score.ToString();
        }

        public void UpdateEnemyCountText(int count)
        {
            enemyInTerritoryCountText.text = count.ToString();
        }

        public void UpdateTimerText(float time)
        {
            var timer = (int)time;
            gameTimeText.text=timer.ToString()+"s";
        }
        #endregion --------------------------------------------------------
    }
}