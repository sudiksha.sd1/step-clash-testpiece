﻿using UnityEngine;
using StepClash.Utils;
using StepClash.Controllers;

namespace StepClash.Views
{ 
    public class StepperUI : BaseClass
    {
        #region ---------------------------- Private Variables ----------------------------
        private StepperController stepperController;
        #endregion --------------------------------------------------------

        #region ---------------------------- Methods ----------------------------
        public void Init(StepperController triadController)
        {
             this.stepperController = triadController;
             SetView();
        }

        private void SetView()
        {
            this.SetView<StepperUI>(this.gameObject);
        }
        #endregion --------------------------------------------------------

        //Not changing material or color in runtime on android...why?????

        /*
    public void SetTriadTile(GameObject tile,int colourIndex)
    {
        //Cheap Hack
        //g.transform.SetParent(tile.transform.parent);
        //g.transform.localPosition = tile.transform.localPosition;
      //  GameManager.instance.text2.text = tile.transform.position.ToString();
        //g.transform.localPosition = tile.transform.localPosition;
        // SetColourToTheTile
        //Color triadColor;
        //var colorEnum = (TriadColor)colourIndex;
        //ColorUtility.TryParseHtmlString(colorEnum.ToString(), out triadColor);
        //// Not working in build
        // triadTile.GetComponent<Renderer>().material.color = triadColor;
        // triadTile.MeshRenderer.sharedMaterial = Instantiate(_map.TileMaterial);
        // tile.GetComponent<UnityTile>().MeshRenderer.sharedMaterial = Instantiate(StepClash.Managers.GameManager.instance.mat); ;
        // Rough hack
        // GameObject tileA = Instantiate(GameManager.instance.triadT, new Vector3(tile.transform.position.x, tile.transform.position.y+10,tile.transform.position.z), tile.transform.rotation);
        //  Material mat = Resources.Load("Material/TileA.mat", typeof(Material)) as Material;
        // tileA.GetComponent<MeshRenderer>().material = mat;
        //tileA.name = "Clan Tile";
    }
        */

    }
}