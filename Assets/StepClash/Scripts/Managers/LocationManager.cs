﻿using Mapbox.Map;
using UnityEngine;
using Mapbox.Utils;
using Mapbox.Unity.Map;
using Mapbox.Unity.Location;
using System.Collections.Generic;


namespace StepClash.Managers
{
    public class LocationManager : MonoBehaviour
    {

        #region ---------------------------- Private Variables ----------------------------
        private AbstractMap map;
        #endregion --------------------------------------------------------


        #region ---------------------------- Public Variables ----------------------------
        public static LocationManager instance;
        public Vector2d curLocation;
        public List<UnwrappedTileId> activeTiles;
        public UnwrappedTileId curTile;
        public UnwrappedTileId prevTile;
        #endregion --------------------------------------------------------


        #region ---------------------------- Private Methods ----------------------------
        private void Awake()
        {
            if (instance != null)
            {
                DestroyImmediate(instance);
                return;
            }
            instance = this;
            DontDestroyOnLoad(instance);
        }
        #endregion --------------------------------------------------------


        #region ---------------------------- Public Methods ----------------------------
        public void SetMap()
        {
            map = GameObject.Find("Map").GetComponent<AbstractMap>();
        }
        public AbstractMap GetMap()
        {
            return map;
        }

        public void SetLocation()
        {
            curLocation = GameObject.Find("Editor").GetComponent<EditorLocationProvider>().LatitudeLongitude;
        }

        public void TileChange()
        {
            prevTile = curTile;
            StartCoroutine(GameManager.instance.ChangeTile());
        }
        #endregion --------------------------------------------------------
    }
}