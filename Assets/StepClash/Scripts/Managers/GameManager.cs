﻿using UnityEngine;
using StepClash.Utils;
using StepClash.Models;
using System.Collections;
using StepClash.Controllers;
using System.Collections.Generic;

namespace StepClash.Managers
{
    public class GameManager : BaseClass
    {
        #region ---------------------------- Private Variables ----------------------------
        private bool isInGame;
        #endregion --------------------------------------------------------



        #region ---------------------------- Public Variables ----------------------------
        public static GameManager instance;
        public GameObject controllers;
        public List<Material> stepperTileMat;
        public bool isMapInitialized = false;
        public bool IsInGame
        {
            get {return isInGame; }
            set { isInGame = value; }
        }
        #endregion --------------------------------------------------------


        #region ---------------------------- Private Methods ----------------------------
        private void Awake()
        {
            if (instance != null)
            {
                DestroyImmediate(instance);
                return;
            }
            instance = this;
            DontDestroyOnLoad(instance);
        }

        private void Start()
        {
            Init();
        }

        private void Init()
        {
            IsInGame = false;
            CreateControllers();
        }

        private void CreateControllers()
        {
            controllers = new GameObject("Controllers");
            controllers.AddComponent<GameController>();
            controllers.AddComponent<StepperController>();
        }
        #endregion --------------------------------------------------------


        #region ---------------------------- Public Methods ----------------------------
        public IEnumerator ChangeTile()
        {
            yield return new WaitForEndOfFrame();
            controllers.GetComponent<StepperController>().ChangeTile();
        }

        public StepperData GetStepper(int index)
        {
            return controllers.GetComponent<StepperController>().GetStepper(index);
        }

        public StepperData GetStepperFromTile(string tile)
        {
            return controllers.GetComponent<StepperController>().GetStepperFromTile(tile);
        }

        public bool IsTileActive(string tile)
        {
            return controllers.GetComponent<StepperController>().IsTileActive(tile);
        }
        #endregion --------------------------------------------------------
    }
}