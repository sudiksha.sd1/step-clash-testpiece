﻿using StepClash.Utils;
using Unity.Notifications.Android;

namespace StepClash.Managers
{
    public class NotificationManager : BaseClass
    {
        #region ---------------------------- Public Variables ----------------------------
        public static NotificationManager instance;
        #endregion --------------------------------------------------------

        #region ---------------------------- Private Methods ----------------------------
        private void Awake()
        {
            if (instance != null)
            {
                DestroyImmediate(instance);
                return;
            }
            instance = this;
            DontDestroyOnLoad(instance);
        }
        #endregion --------------------------------------------------------

        #region ---------------------------- Public Methods ----------------------------
        public string CreateChannel()
        {
            var channel = new AndroidNotificationChannel()
            {
                Id = "channel_id",
                Name = "Stepper Notify",
                Importance = Importance.Default,
                Description = "Stepper Status",
            };
            AndroidNotificationCenter.RegisterNotificationChannel(channel);
            return channel.Id;
        }

        public void CreateNotification(string channel_id,string title,string desc)
        {
            var notification = new AndroidNotification();
            notification.Title = title;
            notification.Text = desc;
            notification.FireTime = System.DateTime.Now.AddSeconds(2);
            AndroidNotificationCenter.SendNotification(notification, channel_id);
        }
        #endregion --------------------------------------------------------
    }
}